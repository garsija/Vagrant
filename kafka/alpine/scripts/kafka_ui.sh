#!/bin/bash

sudo mkdir /opt/kafka_ui
curl -SL https://github.com/provectus/kafka-ui/releases/download/v0.5.0/kafka-ui-api-v0.5.0.jar -o /opt/kafka_ui/kafka-ui-api.jar
sudo cp /vagrant/config/kafka_ui_application-local.yml /opt/kafka_ui/kafka_ui_application-local.yml
sudo chown -R kafka:kafka /opt/kafka_ui
sudo chmod +x /opt/kafka_ui/kafka-ui-api.jar

sudo cat << EOF > /opt/kafka_ui/run.sh
#!/bin/bash
java -Dspring.config.additional-location=/opt/kafka_ui/kafka_ui_application-local.yml -jar /opt/kafka_ui/kafka-ui-api.jar > /opt/kafka_ui/kafka_ui.log 2>&1
EOF
chmod +x /opt/kafka_ui/run.sh

sudo cat << EOF > /etc/init.d/kafka_ui
#!/sbin/openrc-run
depend() {
    need net
    after kafka
    }

user=kafka
name="Kafka UI Daemon"
command="/opt/kafka_ui/run.sh"
pidfile="/run/kafka_ui.pid"
EOF

# add new service for kafka ui
#sudo chmod +x /etc/init.d/kafka_ui
#sudo rc-update add kafka_ui
#sudo rc-service kafka_ui start