#!/bin/bash
. /vagrant/config/install.env

#update and uprage
sudo apk update
sudo apk upgrade --available

#install tools
sudo apk add curl
sudo apk add nano
sudo apk add openjdk17
sudo apk add inetutils-telnet

sudo mkdir /opt/kafka

#add group
sudo addgroup -S kafka

#add kafka user
sudo adduser -h /opt/kafka -S kafka -G kafka

#download kafka
curl -SL https://downloads.apache.org/kafka/${KAFKA_DOWNLOAD_VER}/kafka_2.13-${KAFKA_DOWNLOAD_VER}.tgz -o kafka.tgz
mkdir kafka
tar -xvzf kafka.tgz --directory kafka/ --strip 1
sudo cp -R kafka/* /opt/kafka
sudo rm -R kafka/
sudo chown -R kafka:kafka /opt/kafka
sudo -u kafka mkdir -p /opt/kafka/logs
sudo sed -i -e 's#.*log.dirs=.*#log.dirs=/opt/kafka/logs#' /opt/kafka/config/server.properties
sudo sed -i -e 's#.*advertised.listeners=.*#advertised.listeners=PLAINTEXT://localhost:'${KAFKA_PORT}'#' /opt/kafka/config/server.properties

sudo cat << EOF > /etc/init.d/zookeeper
#!/sbin/openrc-run
depend() {
    need net
    after net
    }

name="Zookeeper Daemon"
command="/opt/kafka/bin/zookeeper-server-start.sh"
command_args="-daemon /opt/kafka/config/zookeeper.properties > /opt/kafka/logs/start-zookeeper.log 2>&1"
pidfile="/run/zookeeper.pid"
EOF

sudo cat << EOF > /etc/init.d/kafka
#!/sbin/openrc-run
depend() {
    need net
    after zookeeper
    }

name="Kafka Daemon"
command="/opt/kafka/bin/kafka-server-start.sh"
command_args="-daemon /opt/kafka/config/server.properties > /opt/kafka/logs/start-kafka.log 2>&1"
pidfile="/run/kafka.pid"
EOF

# add new service for zookeeper
sudo chmod +x /etc/init.d/zookeeper
sudo rc-update add zookeeper
sudo rc-service zookeeper start

# add new service for kafka
sudo chmod +x /etc/init.d/kafka
sudo rc-update add kafka
sudo rc-service kafka start