#!/bin/bash

sudo mkdir /opt/kafka_ui
curl -SL https://github.com/provectus/kafka-ui/releases/download/v0.5.0/kafka-ui-api-v0.5.0.jar -o /opt/kafka_ui/kafka-ui-api.jar
sudo cp /vagrant/config/kafka_ui_application-local.yml /opt/kafka_ui/kafka_ui_application-local.yml
sudo chown -R kafka:kafka /opt/kafka_ui

sudo cat << EOF > /etc/systemd/system/kafka_ui.service
[Unit]
Description=Kafka ui

Requires=network.target remote-fs.target zookeeper.service kafka.service
After=network.target remote-fs.target zookeeper.service kafka.service

[Service]
Type=simple
User=kafka
ExecStart=java -Dspring.config.additional-location=/opt/kafka_ui/kafka_ui_application-local.yml -jar /opt/kafka_ui/kafka-ui-api.jar > /opt/kafka_ui/kafka_ui.log 2>&1
SuccessExitStatus=143
Restart=on-abnormal

[Install]
WantedBy=multi-user.target
EOF

sudo systemctl daemon-reload
sudo systemctl enable kafka_ui
sudo systemctl start kafka_ui