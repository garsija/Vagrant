#!/bin/bash
. /vagrant/config/install.env

#update and uprage
sudo apt-get update
sudo apt-get upgrade -y

#install tools
sudo apt-get install -y curl nano openjdk-17-jre-headless

mkdir /opt/kafka

#add kafka user
sudo useradd -r -d /opt/kafka -s /usr/sbin/nologin kafka

#download kafka
curl -SL https://downloads.apache.org/kafka/${KAFKA_DOWNLOAD_VER}/kafka_2.13-${KAFKA_DOWNLOAD_VER}.tgz -o kafka.tgz
mkdir kafka
tar -xvzf kafka.tgz --directory kafka/ --strip 1
sudo cp -R kafka/* /opt/kafka
sudo rm -R kafka/
sudo chown -R kafka:kafka /opt/kafka
sudo -u kafka mkdir -p /opt/kafka/logs
sudo sed -i -e 's#.*log.dirs=.*#log.dirs=/opt/kafka/logs#' /opt/kafka/config/server.properties
sudo sed -i -e 's#.*advertised.listeners=.*#advertised.listeners=PLAINTEXT://localhost:'${KAFKA_PORT}'#' /opt/kafka/config/server.properties

sudo cat << EOF > /etc/systemd/system/zookeeper.service
[Unit]
Requires=network.target remote-fs.target
After=network.target remote-fs.target

[Service]
Type=simple
User=kafka
ExecStart=/opt/kafka/bin/zookeeper-server-start.sh /opt/kafka/config/zookeeper.properties
ExecStop=/opt/kafka/bin/zookeeper-server-stop.sh
Restart=on-abnormal

[Install]
WantedBy=multi-user.target
EOF

sudo cat << EOF > /etc/systemd/system/kafka.service
[Unit]
Requires=zookeeper.service
After=zookeeper.service

[Service]
Type=simple
User=kafka
ExecStart=/bin/sh -c '/opt/kafka/bin/kafka-server-start.sh /opt/kafka/config/server.properties > /opt/kafka/logs/start-kafka.log 2>&1'
ExecStop=/opt/kafka/bin/kafka-server-stop.sh
Restart=on-abnormal

[Install]
WantedBy=multi-user.target
EOF

sudo systemctl daemon-reload
sudo systemctl enable zookeeper
sudo systemctl start zookeeper

sudo systemctl enable kafka
sudo systemctl start kafka
