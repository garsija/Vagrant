#!/bin/bash
. /vagrant/config/install.env

echo test
if [ -e /vagrant/config/kafka_topics.txt ]
then
    echo "Create topics..."
    awk -F':' '{ system("/opt/kafka/bin/kafka-topics.sh --create --bootstrap-server localhost:'${KAFKA_PORT}' --topic=" $1  ) }' /vagrant/config/kafka_topics.txt
fi
